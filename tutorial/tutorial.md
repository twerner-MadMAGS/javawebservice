# Java Webservice using Spring and IntellijIDEA
In this tutorial I use an IntellijIDEA Gradle project.

#### Setting up the project
To start, create a new Gradle project with java plugin.
![Alt text](images/newProject.png)
This builds a initial project structure including gradle buildscript.
To finalize the project structure we create the location for our Java classes.
The final location should be:
```
{projectRoot}/src/main/java/{projectPackage}
```
In Intellij src/main/java is a predefined path, which should mark your java folder as a source folder for Java(folder appears in blue). If the path is not automaticly recognized, you can set the path in "File | Project Structure | Modules | Sources".

{projectPackage} is a Java-package, which will contain our Java files. This greatly simplyfies the Spring setup.

#### Setting up Gradle
The first thing we need to do is to import the spring-boot plugin for Gradle.
To do this open the build.gradle file and insert
```java
buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:1.2.7.RELEASE")
    }
}
```
at the very top of the file.
To apply the changes open the Gradle toolbox located on the right border of Intellij, or the open it through "View | Tool Windows | Gradle", and hit "Refresh".

Next apply the plugins, needed to build this project:
```java
apply plugin: 'war'
apply plugin: 'spring-boot'
```

Unfortunately Gradle is not smart enough to find our future main-class by itself, so we need to tell it the exact location.
```java
mainClassName = "{projectPackage}.{MainClassName}"
```
In my example I use
```java
mainClassName = "webservice.Main"
```
Spring deploys our webservice using a "war" file. To compile this file, we need to create a gradle task.
```java
war { //task 'war' is predefined, and just needs some configuration
    baseName = 'dsWebservice'
    version = '0.1'
}
```
Finally we add the spring dependency under "dependencies".
Note that the buildscript dependencies differ from the project dependencies, so make sure, to add this to the project dependencies.
```java
compile 'org.springframework.boot:spring-boot-starter-web'
```
The final result should look like this:
```java
group 'ds'
version '1.0-SNAPSHOT'

//Gradle buildscript dependencies:
buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        //enables the just the spring-boot plugin for gradle
        classpath("org.springframework.boot:spring-boot-gradle-plugin:1.2.7.RELEASE")
    }
}

apply plugin: 'java'
apply plugin: 'war' //project will be compiled into war to enable deploying as a webservice
apply plugin: 'spring-boot'

mainClassName = "webservice.Main"
sourceCompatibility = 1.5

war { //task 'war' is predefined, and just needs some configuration
    baseName = 'dsWebservice'
    version = '0.1'
}

repositories {
    mavenCentral()
}

dependencies {
    testCompile group: 'junit', name: 'junit', version: '4.11'
    //The spring framework
    compile 'org.springframework.boot:spring-boot-starter-web'
}
```

#### Create the Java files
Spring is annotation-driven, that means that most the features of Spring are used by simple lines of "@someAnnotation" in our Java-code. Spring automatically scans our files for this annotations and spares us a lot of work.

Per convention every file that handles requests in our webservice is called a controller. Given this we should name the following class something ending with "controller". I use the name exampleController.

After creating the class, the first thing we need to do, is telling Spring that this class is indeed a controller class.
To do this add a annotation to your class-definition:
```java
@Controller
public class exampleController {
...
}
```
To make our webservice accessable we need to bind certain URLs to functions is our project, so that, when someone calls a URL on our server a corresponding function is executed.
Spring does this through the RequestMapping annotation.
To start with an easy example we can create a function in our controller class like follows:
```java
@RequestMapping("/getMsg")
@ResponseBody
public String getMsg() {
    return "someTestString";
}
```
Note that RequestMapping defines a URL and binds it to a function. The name of the function does not have to reflect the name of the URL.

ResponseBody will ensure, that a simple string is returned by the function.
Spring contains a complex binding-strucure, in which a single RequestMapping annotation always tries to return a full HMTL webpage. However, this will not be covered by this tutorial.

The final result should look like this
```java
package webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

//Controller annotation informs spring, that this class contains URL to function mappings
@Controller
public class exampleController {

    //RequestMapping is the central annotation, that binds a URL to a funtion
    @RequestMapping("/getMsg")
    @ResponseBody
    public String getMsg() {
        //ResponseBody tells Spring that the response is not HTML, but a simple String
        return "someTestString";
    }
}

```

#### Starting the server
To start the server we need to create a very simple main-function. In this example I create a "Main"-class to contain this function. However, the name of the class has to reflect the mainClassName stated in the build.gradle file.

The main class also needs some annotations, to enable certain features of Spring:
```java
@ComponentScan
@EnableAutoConfiguration
public class Main {
...
}
```
ComponentScan enables the automatic search for all the Spring-annotations we used in our project.
EnableAutoConfiguration cuts the need, to specify a config file for Spring.

The main-method just contains one line:
```java
public static void main(String[] args) {
    SpringApplication.run(Main.class, args);
}
```
The final result should look like this:
```java
package webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@EnableAutoConfiguration
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
```

The last step of this tutorial will be the creation of the build-configuration in Intellij.

To add a new configuration access "Run | Edit Configurations".

We now add a new Gradle configuration by hitting the green "Plus" and selecting Gradle.
Then we fill in the fields as follows:
![Alt text](images/buildConfig.png)
After hitting OK and selecting the config in the right upper corner of Intellij, we should be able to run the project.

#### Accessing the Webservice
In case the server successfully started, we can access it through our browser, by entering:
```
localhost:8080/getMsg
```
Port 8080 is default and due to our EnableAutoConfiguration annotation.

The result should be a simple string.
![Alt text](images/result.png)

I will provide my source code with this tutorial. The code contains a bunch of different mappings with comments, to display the different types of bindings possible in Spring.
