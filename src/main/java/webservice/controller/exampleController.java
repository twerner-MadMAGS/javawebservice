package webservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by thorben on 09/11/15.
 */

//Controller annotation informs spring, that this class contains URL to function mappings
@Controller
public class exampleController {

    //RequestMapping is the central annotation, that binds a URL to a function
    //localhost:8080/getMsg
    @RequestMapping("/getMsg")
    @ResponseBody
    public String getMsg() {
        //ResponseBody tells Spring that the response is not HTML, but a simple String
        return "someTestString";
    }

    //{msg} is a dynamic URL segment, called PathVariable
    //localhost:8080/getMsg/anyMsgYouWant
    @RequestMapping("/getMsg/{msg}")
    @ResponseBody
    public String returnPathVariable(@PathVariable("msg") String msg) {
        //@PathVariable defines a dynamic URL segment, which can be used in the function
        return "your Msg: " + msg;
    }

    //localhost:8080/postParam?msg=anyMsg
    @RequestMapping("/postParam")
    @ResponseBody
    public String returnURLParam(@RequestParam("msg") String msg) {
        //@RequestParam extracts a URL parameter, which can be used in the function
        return "your Msg: " + msg;
    }

    @RequestMapping("/website")
    @ResponseBody
    public String showWebsite() {
        //When the returned String is HTML the browser should
        //automaticly contruct the website
        return "<html>" +
                    "<head>" +
                        "<title>Some Title</title>" +
                    "</head>" +
                    "<body>" +
                        "<h1>Hello, World!</h1>" +
                        "<p>Paragraph 1</p>" +
                        "<p>Paragraph 2</p>" +
                    "</body>" +
                "</html>";
    }
}
